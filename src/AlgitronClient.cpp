#include "itk-fos-control/AlgitronClient.h"
#include <netdb.h>
#include <iostream>
#include <sstream>
#include <cstring>
#include <termios.h>
#include <unistd.h>

using namespace std;

AlgitronClient::AlgitronClient(){
  m_verbose=false;
  m_user="admin";
  m_pass="admin";
  m_connected=false;
}

AlgitronClient::~AlgitronClient(){
  if(m_connected) Disconnect();
}

bool AlgitronClient::Connect(string host, uint32_t port){

  m_socket = socket(AF_INET , SOCK_STREAM , 0);
  if(m_socket==-1){
    if(m_verbose) cout << "Could not create socket" << endl;
    return false;
  }

  struct hostent * h = gethostbyname(host.c_str());
  if(h==NULL){
    if(m_verbose) cout << "Host not found: " << host << endl;
    return false;
  }

  memcpy(&m_server.sin_addr, h->h_addr, h->h_length);
  m_server.sin_family = AF_INET;
  m_server.sin_port = htons(port);

  if(m_verbose) cout << "Connecting to: " << host << endl;
  if(connect(m_socket, (struct sockaddr *)&m_server , sizeof(m_server)) < 0) {
    if(m_verbose) cout << "Cannot connect to : " << host << endl;
    return false;
  }

  //static struct termios tlocal;
  //cfmakeraw(&tlocal);
  //tcsetattr(STDIN_FILENO,TCSANOW,&tlocal);
  
  //Disable Nagle's algorithm
  int nonagle = 1;
  setsockopt(m_socket,IPPROTO_TCP, nonagle, (char *)&nonagle, sizeof(nonagle));
  //Set timeout to 0.5s
  SetTimeout(500);
  
  //Authenticate
  if(m_verbose) cout << "Authenticate" << endl;
  string cmd=m_user+" "+m_pass+"\r";
  string reply=WriteAndRead(cmd);
  cout << reply << endl;

  m_connected=true;
  return true;
}

void AlgitronClient::Disconnect(){
  if(m_verbose) cout << "quit" << endl;
  WriteAndRead("quit\r");
  close(m_socket);
}

void AlgitronClient::SetCredentials(string user, string pass){
  m_user=user;
  m_pass=pass;
}
  

void AlgitronClient::SetTimeout(uint32_t milis){
  struct timeval tv;
  tv.tv_sec = (int)milis/1000;
  tv.tv_usec = (int)(milis%1000)*1000;
  setsockopt(m_socket,SOL_SOCKET,SO_RCVTIMEO,&tv,sizeof(tv));
}

string AlgitronClient::WriteAndRead(string command){
  send(m_socket,command.c_str(),command.length(),0);
  char* buf=new char[1000];
  int nb=read(m_socket,buf,1000);
  if(nb<0){
    cout << "Error reading from socket" << endl;
  }else if(nb==0){
    cout << "Nothing to read" << endl;
  }else{
    cout << "Read nbytes: " << nb << endl;
    cout << buf << endl;
  }
  return string(buf);
}
 
uint32_t AlgitronClient::GetChannel(){
  if(m_verbose) cout << "Get channel" << endl;
  string reply=WriteAndRead("Getsw\r\n");
  return 0;
}

void AlgitronClient::SetChannel(uint32_t channel){
  if(m_verbose) cout << "Set channel" << endl;
  ostringstream os;
  os << "Setsw " << channel << "\r\n";
  string reply=WriteAndRead(os.str());
}

void AlgitronClient::SetVerbose(bool enable){
  m_verbose=enable;
}
