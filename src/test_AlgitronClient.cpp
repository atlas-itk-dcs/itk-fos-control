#include "itk-fos-control/AlgitronClient.h"
#include <iostream>
#include <cstdlib>
#include <string>

using namespace std;

int main(int argc, char *argv[]){


  AlgitronClient * client = new AlgitronClient();
  client->SetVerbose(true);
  client->Connect("192.168.0.3");

  uint32_t wch=4;
  client->SetChannel(wch);
  uint32_t rch=client->GetChannel();
  
  delete client;
  
  return 0;
}
