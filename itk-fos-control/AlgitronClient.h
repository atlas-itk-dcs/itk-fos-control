#ifndef ALGITRON_CLIENT
#define ALGITRON_CLIENT

#include <cstdint>
#include <netdb.h>
#include <string>

class AlgitronClient{

 public:

  AlgitronClient();
  ~AlgitronClient();
  bool Connect(std::string address,uint32_t port=23);

  uint32_t GetChannel();
  void SetChannel(uint32_t channel);

  void SetCredentials(std::string user, std::string pass);
  void SetVerbose(bool enable);
  void SetTimeout(uint32_t milis);
  std::string WriteAndRead(std::string command);
  void Disconnect();
  
 private:
  struct sockaddr_in m_server;
  int32_t m_socket;
  std::string m_user;
  std::string m_pass;
  bool m_verbose;
  bool m_connected;
};

#endif
