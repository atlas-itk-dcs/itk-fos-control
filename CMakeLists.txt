tdaq_package()

tdaq_add_library(itk-fos-control src/AlgitronClient.cpp)

tdaq_add_executable(switchToChannel src/switchToChannel.cpp)

tdaq_add_executable(test_AlgitronClient src/test_AlgitronClient.cpp LINK_LIBRARIES itk-fos-control)
